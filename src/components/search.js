import React, { useEffect, useState } from "react";
import { Input, Row, Col, Table } from 'antd';
const { Column, ColumnGroup } = Table;
const { Search } = Input;

const SearchData = () => {
  const [state, setState] = useState({
    items: [],
    filteredData: [],
  });

const {items} = state;

  useEffect(() => {
    fetch('https://jsonplaceholder.typicode.com/todos/')
      .then(res => res.json())
      .then((result) => {
          setState({
            items: result,
            filteredData: result,
          });
        }
      )
    }, []);

 const searchTitle = value => {
  let result = [];
  result = items.filter(data => {
    const title = data.title.toLowerCase();
    return title.search(value) !== -1;
  });
  setState({
   ...state,
   filteredData: result,
  });
};

const searchUserId = value => {
 let result = [];
 if (items !== undefined) {
   result = items.filter(data => {
     const userId = data.userId === value;
     return userId;
   });
 }

 setState({
  ...state,
  filteredData: result,
 });
};

  const searchById = id => {
    var getDataId = [];
    fetch('https://jsonplaceholder.typicode.com/todos/'+id)
      .then(res => res.json())
      .then((result) => {
        getDataId.push({ id: result.id, userId: result.userId, title: result.title})
          setState({
            filteredData: getDataId,
          });
        }
      )
  };

  const handleSearch = (event) => {
    if(isNaN(event.target.value)){ //Valida si la entrada no es un numero
      searchTitle(event.target.value.toLowerCase())
    }else {
      searchUserId(parseInt(event.target.value, 10));
    }

    if (event.target.value === "") {
      fetch('https://jsonplaceholder.typicode.com/todos/')
      .then(res => res.json())
      .then((result) => {
          setState({
            items: result,
            filteredData: result,
          });
        }
      )
    }
  };

  return (
    <>
      <div>
        <Row>
          <Col className="mt-25" md={8} offset={8}>
            <Search
              placeholder="Ingrese, title o User Id"
              size="large"
              onChange={event => handleSearch(event)}
              onSearch={searchById}
            />
          </Col>
        </Row>
        <Row>
          <Col className="mt-25" md={16} offset={4}>
            <Table bordered dataSource={state.filteredData}>
              <Column title="ID" dataIndex="id" key="id" />
              <Column title="User ID" dataIndex="userId" key="iduser" />
              <Column title="Title" dataIndex="title" key="title" />
            </Table>
          </Col>
        </Row>
      </div>
    </>
  );
};

export default SearchData ;
